import { Injectable } from '@angular/core';
import { IDatos } from '../interfaces/citas';

@Injectable({
  providedIn: 'root'
})
export class CitaService {
  LISTA_CITAS: IDatos[] = [

    {nombre:'Maritza',apellido:'Abarca',correo:'maeillanes@hotmail.com',celular:67505071,fecha:'14/02/2022',hora:'14:00',descripcion:'hola', password:'eyry5676'},
    {nombre:'Osvaldo',apellido:'Abarca',correo:'osabarca@hotmail.com',celular:67505071,fecha:'14/02/2028',hora:'14:00',descripcion:'hola como estas quisiera conocer mas sobre tu trabajo', password:'y55ry'},
    {nombre:'Elizabeth',apellido:'Armstrong',correo:'elizabetharmstrong39@gmail.com',celular:89897071,fecha:'14/02/2028',hora:'15:00',descripcion:'hola como estas quisiera conocer mas sobre tu trabajo', password:'yeyeryt'},
    {nombre:'Maria Angelica',apellido:'Berguez',correo:'angelicabergez@gmail.com',celular:67505071,fecha:'14/02/2022',hora:'14:00',descripcion:'hola', password:'y4674'},
    {nombre:'Pablo',apellido:'Calderon',correo:'pablo.calderon.cadiz@gmail.com',celular:67505071,fecha:'14/02/2028',hora:'14:00',descripcion:'hola como estas quisiera conocer mas sobre tu trabajo', password:'dfgb'},
    {nombre:'Lourdes',apellido:'Vela Diaz',correo:'velaLou@gmail.com',celular:89897071,fecha:'14/02/2028',hora:'15:00',descripcion:'hola como estas quisiera conocer mas sobre tu trabajo', password:'fdgfd'},
    {nombre:'Camila',apellido:'Muñoz',correo:'camilamo576@gmail.com',celular:67505071,fecha:'14/02/2022',hora:'14:00',descripcion:'hola', password:'gsdfg'},
    {nombre:'Maria del Pilar',apellido:'Balderrama Flores',correo:'camilamo576@gmail.com',celular:67505071,fecha:'14/02/2028',hora:'14:00',descripcion:'hola como estas quisiera conocer mas sobre tu trabajo', password:'dfgfrfg'},
    {nombre:'Lourdes',apellido:'Vela Diaz',correo:'velaLou@gmail.com',celular:89897071,fecha:'14/02/2028',hora:'15:00',descripcion:'hola como estas quisiera conocer mas sobre tu trabajo', password:'afefhk'}
  ];
  constructor() { }

  getCitas(){
    return this.LISTA_CITAS.slice();
  }

  agregarCita(cita:IDatos){
    this.LISTA_CITAS.push(cita);
  }
}
