import { Component, OnInit, ViewChild } from '@angular/core';
import { IDatos } from 'src/app/interfaces/citas';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { CitaService } from 'src/app/servicios/cita.service';

@Component({
  selector: 'app-agenda-agregar',
  templateUrl: './agenda-agregar.component.html',
  styleUrls: ['./agenda-agregar.component.css']
})

export class AgendaAgregarComponent implements OnInit {
  LISTA_CITAS:IDatos[]=[]

  //Para las columnas
  displayedColumns: string[] = ['nombre', 'apellido', 'correo', 'celular','fecha','hora','descripcion','acciones'];
  dataSource !: MatTableDataSource<any>;

  //Para el paginator
  // @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(private _citasService:CitaService) { }

  
  ngOnInit(): void {
    this.cargarCitas();
  }

  cargarCitas(){
    this.LISTA_CITAS=this._citasService.getCitas();
    this.dataSource=new MatTableDataSource(this.LISTA_CITAS)
  }
  // ngAfterViewInit() {
  //   this.dataSource.paginator = this.paginator;
  // }



  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


}
