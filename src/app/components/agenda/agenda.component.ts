import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, FormBuilder, Validators} from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { IDatos } from 'src/app/interfaces/citas';
import { CitaService } from 'src/app/servicios/cita.service';

@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.css']
})
export class AgendaComponent implements OnInit {
form: FormGroup;
  constructor(private fb:FormBuilder,
    private _citaService:CitaService
    ,private router:Router) {
    this.form=this.fb.group({
      nombre:['',[Validators.required,Validators.minLength(4),Validators.pattern(/^[a-zA-zñÑ\s]+$/)]],
      apellido:['',[Validators.required,Validators.minLength(4),Validators.pattern(/^[a-zA-zñÑ\s]+$/)]],
      correo:['',[Validators.required,Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
      celular:['',[Validators.required,Validators.pattern(/^([0-9])*$/)]],
      fecha:['',[Validators.required]],
      hora:['',[Validators.required]],
      descripcion:['',[Validators.required]],
      password:['',[Validators.required,Validators.minLength(4)]]
    })
   }

  ngOnInit(): void {
  }
  agregarCita(){
    console.log(this.form);
    const cita :IDatos ={
      nombre:this.form.value.nombre,
      apellido:this.form.value.apellido,
      correo:this.form.value.correo,
      celular:this.form.value.celular,
      fecha:this.form.value.fecha,
      hora:this.form.value.hora,
      descripcion:this.form.value.descripcion,
      password:this.form.value.password
    }
    console.log(cita);
    this._citaService.agregarCita(cita);
    this.router.navigate(['/agenda']);

    //     this._snackbar.open('Enviado',' ',{
    //   duration:1500,
    //   horizontalPosition:'center',
    //   verticalPosition:'bottom'
    // });
  }
  get NombreNoValido(){
    return this.form.get('nombre')?.invalid && this.form.get('nombre')?.touched;
  }
  get ApellidoNoValido(){
    return this.form.get('apellido')?.invalid && this.form.get('apellido')?.touched;
  }
  get CorreoNoValido(){
    return this.form.get('correo')?.invalid && this.form.get('correo')?.touched;
   }
   get CelularNoValido(){
    return this.form.get('celular')?.invalid && this.form.get('celular')?.touched;
  } 

 
}
